package ru.t1.akolobov.tm.repository;

import ru.t1.akolobov.tm.api.ICommandRepository;
import ru.t1.akolobov.tm.model.Command;

import static ru.t1.akolobov.tm.constant.ArgumentConst.*;
import static ru.t1.akolobov.tm.constant.TerminalConst.*;

public class CommandRepository implements ICommandRepository {

    private static Command ABOUT = new Command(
            CMD_ABOUT,
            ARG_ABOUT,
            "Display developer info.");

    private static Command VERSION = new Command(
            CMD_VERSION,
            ARG_VERSION,
            "Display application version.");

    private static Command HELP = new Command(
            CMD_HELP,
            ARG_HELP,
            "Display list of terminal commands.");

    private static Command INFO = new Command(
            CMD_INFO,
            ARG_INFO,
            "Display system resources information.");

    private static Command ARGUMENTS = new Command(
            CMD_ARGUMENTS,
            ARG_ARGUMENTS,
            "Display available arguments to run application.");

    private static Command COMMANDS = new Command(
            CMD_COMMANDS,
            ARG_COMMANDS,
            "Display available application commands.");

    private static Command EXIT = new Command(
            CMD_EXIT,
            null,
            "Exit application.");

    private static Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, VERSION, HELP, INFO, ARGUMENTS, COMMANDS, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
